# Set the working application directory
# working_directory "/path/to/your/app"
working_directory "/vagrant/project_new"

# Unicorn PID file location
# pid "/path/to/pids/unicorn.pid"
pid "/vagrant/project_new/pids/unicorn.pid"

# Path to logs
# stderr_path "/path/to/log/unicorn.log"
# stdout_path "/path/to/log/unicorn.log"
stderr_path "/vagrant/project_new/log/unicorn.log"
stdout_path "/vagrant/project_new/log/unicorn.log"

# Unicorn socket
#listen "/tmp/unicorn.[app name].sock"
listen "/tmp/unicorn.project_new.sock"

# Number of processes
# worker_processes 4
worker_processes 2

# Time-out
timeout 30
